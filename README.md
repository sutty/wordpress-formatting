# Wordpress Formatting

Ruby port for WP's formatting functions.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'wordpress-formatting'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install wordpress-formatting

## Usage

### wpautop

WordPress posts are formatted with the `wpautop` function.  To obtain
the same results in Ruby, run:

```ruby
require 'wordpress_formatting/wpautop'

WordpressFormatting.wpautop 'A string'
=> "<p>A string</p>"
```

You can also extend a class:

```ruby
class String
  include WordpressFormatting::Wpautop
end

"A string".wpautop
=> "<p>A string</p>"
```

It `fast_blank` is available, it will try to use it.

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake test` to run the tests. You can also run `bin/console`
for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake
install`. To release a new version, update the version number in
`version.rb`, and then run `bundle exec rake release`, which will create
a git tag for the version, push git commits and tags, and push the
`.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/wordpress-formatting>. This
project is intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

## License

The gem is available as free software under the terms of the GPL2
License.

## Code of Conduct

Everyone interacting in the wordpress-formatting project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).

